# Vault Demo

Follow this https://bitbucket.org/mavenwave/vault-ldap-testing/ if you want to run a local LDAP server.

Commands to run.

```
export VAULT_ADDR=http://localhost:8200
vault status
vault operator init
vault operator unseal  keys
vault status
vault auth enable ldap
vault login root_token
vault auth enable ldap
vault write auth/ldap/config url="ldap://127.0.0.1" userdn="dc=example,dc=org" userattr="cn" groupdn="cn=dev,dc=example,dc=org" starttls="false" binddn="cn=admin,dc=example,dc=org" bindpass=admin
vault policy list
vault write auth/ldap/users/testuser policies=foo,bar,default
vault secrets enable -version=2 kv
vault kv put kv/my-secret my-value=s3cr3t
vault secrets list
vault policy write foo policy-file.hcl
vault secrets enable -max-lease-ttl=30s gcp
vault write gcp/config credentials=@my-credentials.json
vault write gcp/roleset/my-key-roleset project="tf-cloud-test-wqs0" secret_type="service_account_key" \
   bindings=-<<EOF
      resource "//cloudresourcemanager.googleapis.com/projects/tf-cloud-test-wqs0" {
        roles = ["roles/viewer"]
      }
    EOF
vault read gcp/key/my-key-roleset
vault lease revoke

curl http://localhost:8200/auth/ldap
curl \
    --request POST \
    --data '{"password": "test"}' \
    http://127.0.0.1:8200/v1/auth/ldap/login/testuser

curl  --header "X-Vault-Token: ..." http://127.0.0.1:8200/v1/kv/data/my-secret  
```
