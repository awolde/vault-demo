path "kv/*" {
  capabilities = ["read", "list"]  
}

path "gcp/*" {
  capabilities = ["read"]
}
